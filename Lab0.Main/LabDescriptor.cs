﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Studia);
        public static Type B = typeof(Dzienne);
        public static Type C = typeof(Zaoczne);

        public static string commonMethodName = "Oplaty";
    }
}
