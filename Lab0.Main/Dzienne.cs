﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Dzienne : Studia
    {

        public Dzienne()
        {
            nazwa = "Dzienne";
            opłaty = Opłaty.bezpłatne;
        }

        public override Opłaty Oplaty()
        {
            return opłaty;
            //Console.WriteLine("Te studia są bezpłatne. ");
        }
    }
}
