﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public enum Opłaty
    {
        bezpłatne, płatne, nieokreślone
    }
    public class Studia
    {

        protected string nazwa;
        protected Opłaty opłaty;

        public Studia()
        {
            nazwa = "Studia";
            opłaty = Opłaty.nieokreślone;
        }

        public virtual Opłaty Oplaty()
        {
            return opłaty;
        }
        public override string ToString()
        {
            return string.Format("To są studia {0},one są {1} ", nazwa, opłaty);
        }
    }
}
