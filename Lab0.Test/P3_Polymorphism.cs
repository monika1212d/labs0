﻿using System;
using Mono.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using h = PK.Test.Helpers;

using Lab0.Main;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Lab0.Test
{
    [TestFixture]
    [TestClass]
    public class P3_Polymorphism
    {
        [Test]
        [TestMethod]
        public void A_Should_Have_Common_Method()
        {
            h.Should_Have_Method(LabDescriptor.A, LabDescriptor.commonMethodName);
        }

        [Test]
        [TestMethod]
        public void B_Should_Have_Common_Method()
        {
            h.Should_Have_Method(LabDescriptor.B, LabDescriptor.commonMethodName);
        }

        [Test]
        [TestMethod]
        public void C_Should_Have_Common_Method()
        {
            h.Should_Have_Method(LabDescriptor.C, LabDescriptor.commonMethodName);
        }

        [Test]
        [TestMethod]
        public void B_Should_Have_Common_Method_With_Different_Result_Than_A()
        {
            // Arrange
            var a = Activator.CreateInstance(LabDescriptor.A);
            var b = Activator.CreateInstance(LabDescriptor.B);

            // Act
            var ra = LabDescriptor.A.GetMethod(LabDescriptor.commonMethodName).Invoke(a, new object[] { });
            var rb = LabDescriptor.A.GetMethod(LabDescriptor.commonMethodName).Invoke(b, new object[] { });

            // Assert
            Assert.That(rb, Is.Not.EqualTo(ra));
        }

        [Test]
        [TestMethod]
        public void C_Should_Have_Common_Method_With_Different_Result_Than_A()
        {
            // Arrange
            var a = Activator.CreateInstance(LabDescriptor.A);
            var c = Activator.CreateInstance(LabDescriptor.C);

            // Act
            var ra = LabDescriptor.A.GetMethod(LabDescriptor.commonMethodName).Invoke(a, new object[] { });
            var rc = LabDescriptor.A.GetMethod(LabDescriptor.commonMethodName).Invoke(c, new object[] { });

            // Assert
            Assert.That(rc, Is.Not.EqualTo(ra));
        }

        [Test]
        [TestMethod]
        public void C_Should_Have_Common_Method_With_Different_Result_Than_B()
        {
            // Arrange
            var b = Activator.CreateInstance(LabDescriptor.B);
            var c = Activator.CreateInstance(LabDescriptor.C);

            // Act
            var rb = LabDescriptor.A.GetMethod(LabDescriptor.commonMethodName).Invoke(b, new object[] { });
            var rc = LabDescriptor.A.GetMethod(LabDescriptor.commonMethodName).Invoke(c, new object[] { });

            // Assert
            Assert.That(rc, Is.Not.EqualTo(rb));
        }

        [Test]
        [TestMethod]
        public void Program_Should_Invoke_Common_Method_At_Least_3_Times()
        {
            // Arrange
            var count = 0;

            // Act
            var method = typeof(Program).GetMethod("Main");
            foreach (var instr in method.GetInstructions())
            {
                if (instr.OpCode.Name == "callvirt")
                {
                    MethodInfo mi = instr.Operand as MethodInfo;
                    if (mi.Name == LabDescriptor.commonMethodName)
                    {
                        count++;
                    }
                }
            }

            // Assert
            Assert.That(count, Is.AtLeast(3));
        }

        [Test]
        [TestMethod]
        public void Program_Should_Create_Objects_Of_All_Types()
        {
            // Arrange
            var objs = new List<Type>();

            // Act
            var method = typeof(Program).GetMethod("Main");
            foreach (var instr in method.GetInstructions())
            {
                if (instr.OpCode.Name == "newobj")
                {
                    ConstructorInfo ci = instr.Operand as ConstructorInfo;
                    objs.Add(ci.DeclaringType);
                }
            }

            // Assert
            Assert.That(objs, Has.Some.EqualTo(LabDescriptor.A));
            Assert.That(objs, Has.Some.EqualTo(LabDescriptor.B));
            Assert.That(objs, Has.Some.EqualTo(LabDescriptor.C));
        }

        [Test]
        [TestMethod]
        public void Program_Should_Invoke_Methods_Of_B_Only_In_Polymorphic_Way()
        {
            // Arrange
            var text = File.ReadAllText(@"..\..\..\Lab0.Main\Program.cs");
            var ops = new List<string>();

            // Act
            var match = Regex.Match(text, @"(.{3})\s+" + LabDescriptor.B.Name, RegexOptions.Multiline);
            while (match.Success)
            {
                ops.Add(match.Groups[1].Value);
                match = match.NextMatch();
            }

            // Assert
            Assert.That(ops, Has.All.EqualTo("new"));
        }

        [Test]
        [TestMethod]
        public void Program_Should_Invoke_Methods_Of_C_Only_In_Polymorphic_Way()
        {
            // Arrange
            var text = File.ReadAllText(@"..\..\..\Lab0.Main\Program.cs");
            var ops = new List<string>();

            // Act
            var match = Regex.Match(text, @"(.{3})\s+" + LabDescriptor.C.Name, RegexOptions.Multiline);
            while (match.Success)
            {
                ops.Add(match.Groups[1].Value);
                match = match.NextMatch();
            }

            // Assert
            Assert.That(ops, Has.All.EqualTo("new"));
        }
    }
}
